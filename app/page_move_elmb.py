import streamlit as st
from utils import *
from datetime import timedelta

if not check_password(st):
    st.stop()  # Do not continue if check_password is not True.

@st.dialog('Warning!')
def warning_add_elmb():
    '''Display a warning dialog before adding an ELMB to the database.'''
    st.write('Are you sure you want to update the database?')
    st.write('The only way to revert this action is to manually edit the database.')
    st.session_state.add_elmb_continue = False
    if st.button('Update database'):
        st.session_state.add_elmb_continue = True
        st.rerun()
    if st.button('Cancel'):
        st.rerun()

@st.dialog('Warning!')
def warning_move_elmb():
    '''Display a warning dialog before moving ELMBs in the database.'''
    st.write('Are you sure you want to update the database?')
    st.write('The only way to revert this action is to manually edit the database.')
    st.session_state.move_elmb_continue = False
    if st.button('Update database'):
        st.session_state.move_elmb_continue = True
        st.rerun()
    if st.button('Cancel'):
        st.rerun()


st.subheader('Specify ELMB')

serial = st.text_input('ELMB serial (4 characters)', max_chars=4)
if len(serial) < 4:
    st.error('The ELMB serial should be 4 characters long')
    st.stop()

command = f"select count(*) from public.elmbs where serial_num = '{serial}'"
count_elmb = execute_command_db_return_1(command)


if count_elmb == 0:
    st.write('The database does not contain this ELMB yet.')
    elmb_type = st.radio('Type of ELMB', options=['ELMB_v1', 'ELMB2'])
    if st.button('Add ELMB to database', type='primary'):
        warning_add_elmb()
    
    if 'add_elmb_continue' in st.session_state and st.session_state.add_elmb_continue:
        st.session_state.add_elmb_continue = False
    
        command = f"INSERT INTO public.elmbs (serial_num, type) VALUES ('{serial}', '{elmb_type}')"
        execute_command_db(command)
        st.rerun()
    
    st.stop()


st.write('The database already contains this ELMB.')

st.subheader('ELMB configuration')

delta_T = st.number_input('Configuration of ΔT (object dictionary index 0x4000 subindex 4)', min_value=0, max_value=255, value=20)

st.subheader('ELMB location')

experiment, system, rack, position = input_location(st)
location_id = get_location_id(experiment, system, rack, position)


st.subheader('Link to elog')
elog_hyperlink = st.text_input(label='Optionally provide a link to the corresponding elog entry')
if len(elog_hyperlink) == 0:
    elog_hyperlink = 'NULL'

df_elmb_old = execute_command_db_return_df(f'SELECT elmb_id, node_id from latest_states_elmb where location_id = {location_id}')
elmb_id_old = df_elmb_old['elmb_id'][0]
node_id_old = df_elmb_old['node_id'][0]

st.markdown('Press the button below to update the database with the new ELMB. The old ELMB in the given location will be moved to 256.')
if st.button('Move ELMB', type='primary'):
    warning_move_elmb()

if 'move_elmb_continue' in st.session_state and st.session_state.move_elmb_continue:
    st.session_state.move_elmb_continue = False
    
    # Move old ELMB to 256
    location_id_256 = execute_command_db_return_1("SELECT id FROM public.locations WHERE experiment = '256'")
    execute_command_db(f"INSERT INTO public.states_elmb (elmb_id, timestamp, node_id, location_id, elog_hyperlink) VALUES \
        ('{elmb_id_old}', '{datetime.now() - timedelta(seconds=5)}', 0, {location_id_256}, '{elog_hyperlink}')")
    
    # Move new ELMB
    execute_command_db(f"INSERT INTO public.states_elmb (elmb_id, timestamp, node_id, location_id, config_delta_temp, elog_hyperlink) VALUES \
        ('{serial}', '{datetime.now()}', {node_id_old}, {location_id}, {delta_T}, '{elog_hyperlink}')")
