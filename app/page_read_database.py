
from database_operations import *
from utils import *
import streamlit as st
import numpy as np
import matplotlib.pyplot as plt
import math

def get_flowcell_df(df_flowcell_states):
    '''Return a DataFrame with flowcell ids in the right locations.'''
    location_to_ids = dict() # Map (CH, is_input) to flowcell id(s)
    for _, row in df_flowcell_states.iterrows():
        loc = (row['channel'], row['is_input'])
        location_to_ids[loc] = location_to_ids.get(loc, [])
        location_to_ids[loc].append(str(row['flowcell_id']))
    
    arr_locs = np.empty((2,18), dtype=object)
    for loc in location_to_ids:
        ind2 = loc[0]-1 # Index based on channel number
        ind1 = int(loc[1]==False)
        arr_locs[ind1,ind2] = ', '.join(location_to_ids[loc])
    # Determine number of channels with at least one flowcell installed
    for i in range(17):
        if arr_locs[0, 17-i] != None or arr_locs[1, 17-i] != None:
            break
    nb_active_channels = 18-i
    # Return dataframe
    arr_locs = arr_locs[:, :nb_active_channels]
    return pd.DataFrame.from_records(arr_locs, index=['Input', 'Output'], columns=[f'CH{i+1}' for i in range(nb_active_channels)])
        

## Menu to select location
st.sidebar.subheader('Select location')

experiment, system, rack, position = input_location(st.sidebar)


st.subheader(f'{experiment} {system}: ELMB CAN-bus node IDs')

df_node_ids = execute_command_db_return_df(dedent(f'''\
    select rack, position, node_id from latest_states_elmb lse
    join locations l on l.id = lse.location_id 
    where experiment = '{experiment}' and system = '{system}'
    order by rack, position
'''))

st.write(df_node_ids)


st.subheader('Flowcell locations')

location_id = get_location_id(experiment, system, rack, position)
df_flowcell_states = execute_command_db_return_df(
    f'select * from latest_states_flowcell lsf where lsf.location_id = {location_id}'
)
df = get_flowcell_df(df_flowcell_states)

st.table(df)


st.subheader('Calibrations')
show_summary_calib = st.checkbox('Show summary', value=True)

df_calibrations = execute_command_db_return_df(dedent(f'''\
    select *
    from latest_states_flowcell lsf
    join calibrations c on c.flowcell_id=lsf.flowcell_id and c.id = lsf.active_calibration_id
    where lsf.location_id = {location_id}
    order by is_input desc, channel
'''))

df_calibrations_shown = execute_command_db_return_df(dedent(f'''\
    select {'lsf.flowcell_id, calibration_gas, low_flow_min, low_flow_max, high_flow_enabled, high_flow_max, config_delta_temp, c.timestamp as time_calibration, lsf.timestamp as time_state_flowcell' if show_summary_calib else '*'}
    from latest_states_flowcell lsf
    join calibrations c on c.flowcell_id=lsf.flowcell_id and c.id = lsf.active_calibration_id
    where lsf.location_id = {location_id}
    order by is_input desc, channel
'''))
if show_summary_calib:
    st.write(df_calibrations_shown)
else:
    # Format calibration parameters in scientific notation
    try:
        st.write(df_calibrations_shown.style.format(subset=[f'low_x{i}' for i in range(5)]+[f'high_x{i}' for i in range(5)], formatter="{:.4E}"))
    except TypeError:
        st.write(df_calibrations_shown.style.format(subset=[f'low_x{i}' for i in range(5)], formatter="{:.4E}"))


nb_flowcells = len(df_calibrations)
nb_groups = math.ceil(nb_flowcells / 10)
plot_flowcell_id = st.select_slider(
    'Plot calibration curves',
    options = ['None'] + list(df_calibrations['flowcell_id']) + [f'Group of 10 number {i+1}/{nb_groups}' for i in range(nb_groups)] + ['All'],
    value='All',
    help = 'Select the flowcell IDs for which to plot the calibration curve. Use the left and right arrow keys for quick navigation.'
)

def plot_curve_for_id(flowcell_id, single_plot=True):
    df_calibration_row = df_calibrations[df_calibrations.flowcell_id == flowcell_id].iloc[0]
    plot_curve(df_calibration_row, single_plot)

fig = plt.figure()

if plot_flowcell_id != 'None':
    if plot_flowcell_id == 'All':
        for id in df_calibrations['flowcell_id']:
            plot_curve_for_id(id, single_plot=False)
    elif 'Group' in str(plot_flowcell_id):
        temp = plot_flowcell_id.split(' ')[-1]
        nb_group = int(temp.split('/')[0])
        ids = list(df_calibrations['flowcell_id'])
        for id in ids[10*nb_group-10:10*nb_group]:
            plot_curve_for_id(id, single_plot=False)
    else:
        plot_curve_for_id(plot_flowcell_id)

    show_fig(fig)
