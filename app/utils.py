from database_operations import *
import matplotlib.pyplot as plt
import numpy as np

def input_location(streamlit_object):
    '''Display input elements to select an ELMB location'''
    
    df_experiments = execute_command_db_return_df(
        'select distinct experiment from locations order by experiment'
    )
    experiment = streamlit_object.selectbox('Experiment', df_experiments[df_experiments.experiment != '256'])

    df_systems = execute_command_db_return_df(
        f"select distinct system from locations where experiment = '{experiment}' order by system"
    )
    system = streamlit_object.selectbox('System', df_systems)

    df_racks = execute_command_db_return_df(
        f"select distinct rack from locations where experiment = '{experiment}' and system = '{system}' order by rack"
    )
    rack = streamlit_object.selectbox('Rack', df_racks)

    df_positions = execute_command_db_return_df(
        f"select distinct position from locations where experiment='{experiment}' and system='{system}' and rack='{rack}' order by position"
    )
    position = streamlit_object.selectbox('Position', df_positions)

    return experiment, system, rack, position



def check_password(streamlit_object):
    """Returns `True` if the user had the correct password."""

    if len(sys.argv) > 1 and sys.argv[1] == 'use_streamlit_secrets':
        # Use password saved locally in .streamlit/secrets.toml
        password = streamlit_object.secrets['STREAMLIT_PASSWORD']
    else:
        # Use password saved in environment variable
        password = os.getenv('STREAMLIT_PASSWORD')

    def password_entered():
        """Checks whether a password entered by the user is correct."""
        if streamlit_object.session_state['password'] == password:
            streamlit_object.session_state["password_correct"] = True
            del streamlit_object.session_state["password"]  # Don't store the password.
        else:
            streamlit_object.session_state["password_correct"] = False

    # Return True if the password is validated.
    if streamlit_object.session_state.get("password_correct", False):
        return True

    # Show input for password.
    st.markdown(':red[Enter the password to edit the database]')
    streamlit_object.text_input(
        "Password", type="password", on_change=password_entered, key="password"
    )
    if "password_correct" in streamlit_object.session_state:
        streamlit_object.error("😕 Password incorrect")
    return False


def plot_curve(df_calibration_row, single_plot=True, custom_plot_label=None):
    '''Plot the calibration curve for the given calibration row from the database.
    Depending on the boolean single_plot, the style of the plot is different.'''
    flowcell_id = df_calibration_row['flowcell_id']
    row = df_calibration_row
    params_low = [row[s] for s in ['low_x0', 'low_x1', 'low_x2', 'low_x3', 'low_x4']]
    low_flow_min = row['low_flow_min']
    low_flow_max = row['low_flow_max']
    slope_low_flow_min = row['slope_low_flow_min']
    slope_low_flow_max = row['slope_low_flow_max']
    
    xx = list(np.linspace(slope_low_flow_max, slope_low_flow_min, 50))
    yy = [np.dot(params_low, [1, x, x**2, x**3, x**4]) for x in xx]
    
    high_flow_enabled = row['high_flow_enabled']
    if high_flow_enabled:
        high_flow_max = row['high_flow_max']
        slope_high_flow_max = row['slope_high_flow_max']
        params_high = [row[s] for s in ['high_x0', 'high_x1', 'high_x2', 'high_x3', 'high_x4']]
        xx2 = list(np.linspace(slope_high_flow_max, slope_low_flow_max, 50))
        yy2 = [np.dot(params_high, [1, x, x**2, x**3, x**4]) for x in xx2]

    if single_plot:
        plt.plot(xx, yy, label='Low flow')
        if high_flow_enabled:
            plt.plot(xx2, yy2, label='High flow')
        plt.title(f'Calibration flowcell {flowcell_id}')
        limit_values_xx = [slope_low_flow_max, slope_low_flow_min]
        limit_values_yy = [low_flow_max, low_flow_min]
        if high_flow_enabled:
            limit_values_xx.append(slope_high_flow_max)
            limit_values_yy.append(high_flow_max)
        plt.plot(limit_values_xx, limit_values_yy, 'r.', markersize=7, label='Limit values (curve ends)')
    else:
        if not high_flow_enabled:
            xx2 = yy2 = []
        plot_label = custom_plot_label if custom_plot_label else f'{flowcell_id}'
        plt.plot(xx2+xx, yy2+yy, label=plot_label, alpha=0.7)

def show_fig(fig):
    plt.xlabel('Slope (m°C/s)')
    plt.ylabel('Flow (normal l/h)')
    plt.legend()
    plt.grid()
    st.pyplot(fig)

