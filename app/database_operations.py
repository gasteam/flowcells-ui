import streamlit as st
import psycopg2
from textwrap import dedent
import pandas as pd
import sys
import os
from datetime import datetime

@st.cache_resource(ttl='1h')
def get_db_conn(write_access=False):
    user = 'admin' if write_access else 'readonly_user'
    if len(sys.argv) > 1 and sys.argv[1] == 'use_streamlit_secrets':
        # Use passwords saved locally in .streamlit/secrets.toml
        password = st.secrets['PW_FLOWCELLS_DB_ADMIN' if write_access else 'PW_FLOWCELLS_DB']
    else:
        # Use passwords saved in environment variables
        password = os.getenv('PW_FLOWCELLS_DB_ADMIN' if write_access else 'PW_FLOWCELLS_DB')
    db_conn = psycopg2.connect(
        host='dbod-flowcells.cern.ch',
        database='flowcells',
        user=user,
        port=6604,
        password=password
    )
    return db_conn


def execute_command_db(command: str):
    with get_db_conn(write_access=True) as db_conn:
        with db_conn.cursor() as cur:
            cur.execute(command)

def execute_command_db_return_1(command: str):
    with get_db_conn(write_access=True) as db_conn:
        with db_conn.cursor() as cur:
            cur.execute(command)
            try:
                return cur.fetchall()[0][0]
            except:
                raise Exception(f'SQL query has empty result! Query:\n{command}')

def execute_command_db_return_df(command: str):
    '''Execute the query and return a pandas DataFrame'''
    with get_db_conn() as db_conn:
        with db_conn.cursor() as cur:
            cur.execute(command)
            data = cur.fetchall()
            col_names = [e[0] for e in cur.description]
            # Get rid of duplicate column names
            for i in range(1, len(col_names)):
                name = col_names[i]
                for j in range(i):
                    if name == col_names[j]:
                        name += '_bis'
                col_names[i] = name
            return pd.DataFrame(data, columns=col_names)


def get_sql_str(s: str):
    '''Return given string between apostrophes, but return NULL if string is empty or NULL'''
    s = s.replace("'", '"')
    if not s or s=='NULL':
        return 'NULL'
    else:
        return f"'{s}'"


def add_flowcell(id_barcode: int, channel_size: float = 'NULL'):
    '''Add a new flowcell to the database if it does not exist yet.'''
    command = dedent(f'''\
    INSERT INTO public.flowcells
    (id_barcode, channel_size)
    VALUES ({id_barcode}, {channel_size})
    ON CONFLICT DO NOTHING
    ''')
    execute_command_db(command)


def add_elmb(serial_num: str, type: str):
    '''Add a new elmb to the database'''
    command = dedent(f'''\
    INSERT INTO public.elmbs
    (serial_num, type)
    VALUES ({get_sql_str(serial_num)}, {get_sql_str(type)})
    ''')
    execute_command_db(command)


def add_motherboard(id_sticker: str, type: str = 'NULL'):
    '''Add a new motherboard to the database'''
    command = dedent(f'''\
    INSERT INTO public.motherboards
    (id_sticker, type)
    VALUES ({get_sql_str(id_sticker)}, {get_sql_str(type)})
    ''')
    execute_command_db(command)


def add_calibration(
    flowcell_id: int,
    timestamp: datetime,
    calibration_gas: str,
    low_flow_min: float,
    low_flow_max: float,
    slope_low_flow_min: int,
    slope_low_flow_max: int,
    low_x0: float,
    low_x1: float,
    low_x2: float,
    low_x3: float,
    low_x4: float,
    high_flow_enabled: bool = False,
    high_flow_max: float = 'NULL',
    slope_high_flow_max: int = 'NULL',
    high_x0: float = 'NULL',
    high_x1: float = 'NULL',
    high_x2: float = 'NULL',
    high_x3: float = 'NULL',
    high_x4: float = 'NULL',
    motherboard_id: str = 'NULL',
    elmb_id: str = 'NULL',
    channel_pressure_gauge: float = 'NULL',
    config_delta_temp: int = 'NULL'
) -> int:
    '''Add a new calibration to the database and return its id'''
    if high_flow_enabled:
        assert(high_flow_max != 'NULL')
        assert(slope_high_flow_max != 'NULL')
        assert(high_x0 != 'NULL')
        assert(high_x1 != 'NULL')
        assert(high_x2 != 'NULL')
        assert(high_x3 != 'NULL')
        assert(high_x4 != 'NULL')
    command = dedent(f'''\
    INSERT INTO public.calibrations
    (
        motherboard_id,
        elmb_id,
        flowcell_id,
        timestamp,
        calibration_gas,
        channel_pressure_gauge,
        low_flow_min,
        low_flow_max,
        slope_low_flow_min,
        slope_low_flow_max,
        low_x0,
        low_x1,
        low_x2,
        low_x3,
        low_x4,
        high_flow_enabled,
        high_flow_max,
        slope_high_flow_max,
        high_x0,
        high_x1,
        high_x2,
        high_x3,
        high_x4,
        config_delta_temp
    )
    VALUES (
        {get_sql_str(motherboard_id)},
        {get_sql_str(elmb_id)},
        {flowcell_id},
        {get_sql_str(timestamp.strftime('%Y/%m/%d %H:%M:%S'))},
        {get_sql_str(calibration_gas)},
        {channel_pressure_gauge},
        {low_flow_min},
        {low_flow_max},
        {slope_low_flow_min},
        {slope_low_flow_max},
        {low_x0},
        {low_x1},
        {low_x2},
        {low_x3},
        {low_x4},
        {high_flow_enabled},
        {high_flow_max},
        {slope_high_flow_max},
        {high_x0},
        {high_x1},
        {high_x2},
        {high_x3},
        {high_x4},
        {config_delta_temp}
    )
    RETURNING id
    ''')
    return execute_command_db_return_1(command)


def add_calibration_raw_data(
    calibration_id: int,
    flow: float,
    slope1: int,
    slope2: int,
    T_ambient1: int,
    T_ambient2: int,
    I_low_in: int,
    I_high_in: int,
    I_low_out: int,
    I_high_out: int,
    t_wait1: int,
    t_wait2: int,
    t_sample1: int,
    t_sample2: int,
    status: int = 'NULL'
):
    '''Add new calibration raw data'''
    command = dedent(f'''\
    INSERT INTO public.calibration_raw_data
    (
        calibration_id,
        flow,
        slope1,
        slope2,
        T_ambient1,
        T_ambient2,
        I_low_in,
        I_high_in,
        I_low_out,
        I_high_out,
        t_wait1,
        t_wait2,
        t_sample1,
        t_sample2,
        status
    )
    VALUES (
        {calibration_id},
        {flow},
        {slope1},
        {slope2},
        {T_ambient1},
        {T_ambient2},
        {I_low_in},
        {I_high_in},
        {I_low_out},
        {I_high_out},
        {t_wait1},
        {t_wait2},
        {t_sample1},
        {t_sample2},
        {status}
    )
    ''')
    execute_command_db(command)


def add_location(
    experiment: str,
    system: str,
    rack: int,
    position: int = 1,
    note: str = 'NULL',
    name_table_access_db: str = 'NULL'
) -> int:
    command = dedent(f'''\
    INSERT INTO public.locations
    (
        experiment,
        system,
        rack,
        position,
        note,
        name_table_access_db
    )
    VALUES (
        {get_sql_str(experiment)},
        {get_sql_str(system)},
        {rack},
        {position},
        {get_sql_str(note)},
        {get_sql_str(name_table_access_db)}
    )
    RETURNING id
    ''')
    return execute_command_db_return_1(command)


def add_state_motherboard(
    motherboard_id: str,
    timestamp: datetime,
    location_id: int,
    elog_hyperlink: str = 'NULL'
):
    '''Add a new state of a motherboard to the database'''
    command = dedent(f'''\
    INSERT INTO public.states_motherboard
    (
        motherboard_id,
        timestamp,
        location_id,
        elog_hyperlink
    )
    VALUES (
        {get_sql_str(motherboard_id)},
        {get_sql_str(timestamp.strftime('%Y/%m/%d %H:%M:%S'))},
        {location_id},
        {get_sql_str(elog_hyperlink)}
    )
    ''')
    execute_command_db(command)


def add_state_elmb(
    timestamp: datetime,
    node_id: int,
    location_id: int,
    elmb_id: str = 'NULL',
    firmware: str = 'NULL',
    config_delta_temp: float = 'NULL',
    config_heartbeat_s: float = 'NULL',
    config_bus_off_counter: int = 'NULL',
    elog_hyperlink: str = 'NULL'
):
    '''Add a new state of an elmb to the database'''
    command = dedent(f'''\
    INSERT INTO public.states_elmb
    (
        elmb_id,
        timestamp,
        firmware,
        node_id,
        location_id,
        config_delta_temp,
        config_heartbeat_s,
        config_bus_off_counter,
        elog_hyperlink
    )
    VALUES (
        {get_sql_str(elmb_id)},
        {get_sql_str(timestamp.strftime('%Y/%m/%d %H:%M:%S'))},
        {get_sql_str(firmware)},
        {node_id},
        {location_id},
        {config_delta_temp},
        {config_heartbeat_s},
        {config_bus_off_counter},
        {get_sql_str(elog_hyperlink)}
    )
    ''')
    execute_command_db(command)


def add_state_flowcell(
    flowcell_id: int,
    timestamp: datetime,
    location_id: int,
    active_calibration_id: int = 'NULL',
    channel: int = 'NULL',
    is_input: bool = 'NULL',
    elog_hyperlink: str = 'NULL'
):
    '''Add a new state of a flowcell to the database'''
    command = dedent(f'''\
    INSERT INTO public.states_flowcell
    (
        flowcell_id,
        timestamp,
        active_calibration_id,
        location_id,
        channel,
        is_input,
        elog_hyperlink
    )
    VALUES (
        {flowcell_id},
        {get_sql_str(timestamp.strftime('%Y/%m/%d %H:%M:%S'))},
        {active_calibration_id},
        {location_id},
        {channel},
        {is_input},
        {get_sql_str(elog_hyperlink)}
    )
    ''')
    execute_command_db(command)



def get_location_id(
    experiment: str,
    system: str,
    rack: int,
    position: int
):
    '''Return the id for the specified location'''
    command = dedent(f'''\
    SELECT id FROM public.locations
    WHERE experiment = {get_sql_str(experiment)}
        AND system = {get_sql_str(system)}
        AND rack = {rack}
        AND position = {position}
    ''')
    return execute_command_db_return_1(command)

def get_location_attributes(location_id: int):
    '''Return the location attributes for the given location_id'''
    command = dedent(f'''\
    SELECT experiment, system, rack, position FROM public.locations
    WHERE id = {location_id}
    ''')
    df = execute_command_db_return_df(command)
    return df['experiment'][0], df['system'][0], df['rack'][0], df['position'][0]

def get_current_state_flowcell(flowcell_id: int):
    '''Return a dataframe with the current state of the given flowcell'''
    command = dedent(f'''\
        select lsf.*
        from public.latest_states_flowcell lsf
        where flowcell_id = {flowcell_id}
    ''')
    return execute_command_db_return_df(command).fillna('NULL')


def change_active_calibration_flowcell(
    flowcell_id: int,
    timestamp: datetime,
    active_calibration_id: int
):
    '''Change the active calibration of a flowcell
    (i.e. the calibration parameters that are used in the gas systems).'''
    current_state = get_current_state_flowcell(flowcell_id)
    add_state_flowcell(
        flowcell_id=flowcell_id,
        timestamp=timestamp,
        location_id=current_state.location_id[0],
        active_calibration_id=active_calibration_id,
        channel=current_state.channel[0],
        is_input=current_state.is_input[0]
    )


def get_calibration_df(flowcell_id: int):
    command = dedent(f'''\
        select *
        from calibrations c
        where flowcell_id = {flowcell_id}
        order by timestamp DESC
    ''')
    return execute_command_db_return_df(command)


def get_most_recent_calibration_id(flowcell_id: int):
    command = dedent(f'''\
        select id
        from calibrations c
        inner join (
            select max(timestamp) timestamp
            from calibrations
            where flowcell_id = {flowcell_id}
        ) temp
        on c.timestamp = temp.timestamp
        where flowcell_id = {flowcell_id}
    ''')
    return execute_command_db_return_1(command)



def update_state_to_most_recent_calibration(flowcell_id: int, timestamp: datetime):
    '''Update the state of the given flowcell to the most recent calibration performed.'''
    calibration_id = get_most_recent_calibration_id(flowcell_id)
    change_active_calibration_flowcell(flowcell_id, timestamp, calibration_id)
    

def move_flowcell(
    flowcell_id: int,
    timestamp: datetime,
    location_id: int,
    channel: int,
    is_input: bool
):
    '''Change the physical location of a flowcell'''
    current_state = get_current_state_flowcell(flowcell_id)
    add_state_flowcell(
        flowcell_id,
        timestamp,
        location_id,
        current_state.active_calibration_id[0],
        channel,
        is_input
    )
