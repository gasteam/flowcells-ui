import streamlit as st
import numpy as np
import pandas as pd
import math
import polyfit
import io
import matplotlib.pyplot as plt
import sys
import os
import gitlab
import base64
from database_operations import *
from utils import *
from datetime import timedelta
import re
import logging
logging.basicConfig(level=logging.INFO)


def get_calibration_filenames():
    gl = gitlab.Gitlab('https://gitlab.cern.ch/')
    project = gl.projects.get('gasteam/calibration-setup-flowcells')
    items = project.repository_tree(path='code/calibration_data', get_all=True)
    filenames = [item['path'].split('/')[-1] for item in items]
    return filenames

@st.cache_data
def get_calibration_data(filename):
    gl = gitlab.Gitlab('https://gitlab.cern.ch/')
    project = gl.projects.get('gasteam/calibration-setup-flowcells')
    items = project.repository_tree(path='code/calibration_data', get_all=True)
    for index_item, item in enumerate(items):
        if item['path'].split('/')[-1] == filename:
            break
    file_all_data = project.repository_blob(items[index_item]['id'])
    content = base64.b64decode(file_all_data['content']).decode('utf-8')
    data_parts = content.split('-----')
    metadata, data_rows = data_parts[1:]
    df = pd.read_csv(io.StringIO(data_rows))
    return metadata, df


st.sidebar.subheader('Calibration file')

filenames = get_calibration_filenames()
filename = st.sidebar.selectbox(
    'Select a calibration file',
    options=sorted(filenames, reverse=True)
)
st.sidebar.markdown('The calibration files are read from [the GitLab repository](https://gitlab.cern.ch/gasteam/calibration-setup-flowcells/-/tree/master/code/calibration_data)')

if st.sidebar.button('Refresh cache for calibration files (after changes to files)'):
    get_calibration_data.clear()
    

metadata, df = get_calibration_data(filename)
barcodes = sorted(list(set(list(df['barcode1']) + list(df['barcode2']))))
calibration_timestamp_str = re.search('calibration_(.*).csv', filename).group(1)
calibration_timestamp = datetime.strptime(calibration_timestamp_str, '%Y-%m-%d_%H-%M-%S')
calibration_gas = re.search('fluid=(.*)\n', metadata).group(1)

if 'is_dual_curve=True' in metadata:
    is_dual_curve = True
elif 'is_dual_curve=False' in metadata:
    is_dual_curve = False
else:
    raise Exception(f'Could not identify boolean is_dual_curve in metadata!')

if 'ELMB_Delta_T' in metadata:
    config_delta_temp = int(re.search('ELMB_Delta_T=(\d+)', metadata).group(1))
else:
    config_delta_temp = 'NULL'


def get_coefficients_polynomial(list_slopes, list_flows, degree, use_constraint_decreasing, use_constraint_convex):
    '''Return the polynomial coefficients for to map the slopes to the flows.
    The polynomial has a degree of at most 4 and there's an optional constraint
    to make the polynomial monotonically decreasing'''
    if use_constraint_decreasing or use_constraint_convex:
        if use_constraint_decreasing and use_constraint_convex:
            constraint = polyfit.Constraints(monotonicity='dec', curvature='convex')
        elif use_constraint_decreasing:
            constraint = polyfit.Constraints(monotonicity='dec')
        else:
            constraint = polyfit.Constraints(curvature='convex')
        polyestimator = polyfit.PolynomRegressor(deg=degree)
        X = np.array(list_slopes).reshape((-1,1))
        y = np.array(list_flows)
        polyestimator.fit(X, y, loss = 'l2', constraints={0: constraint})
        params_low = polyestimator.coeffs_
    else:
        params_low = np.polynomial.polynomial.polyfit(
            x = list_slopes,
            y = list_flows,
            deg = degree
        )
    return np.pad(params_low, [0, 4-degree], mode='constant', constant_values=0.0)
    

@st.cache_data
def process_data(df, degree_polynomial_low_flows, degree_polynomial_high_flows, use_constraint_dec, use_constraint_conv):
    flow_lists_low = dict()
    slope_lists_low = dict()
    calib_params_low = dict()
    flow_lists_high = dict()
    slope_lists_high = dict()
    calib_params_high = dict()
    # Initialize dictionaries
    for barcode in barcodes:
        for d in [flow_lists_low, slope_lists_low, calib_params_low, flow_lists_high, slope_lists_high, calib_params_high]:
            d[barcode] = []
    flow_limits = [-1, -1, -1] # [low_flow_min, low_flow_max, high_flow_max]
    slope_limits = dict() # For every barcode: [slope_low_flow_min, slope_low_flow_max, slope_high_flow_max]
    
    # Read flow and slope values
    for _, row in df.iterrows():
        if row['curve'] == 'low':
            flow_lists_low[row['barcode1']].append(row['flow'])
            flow_lists_low[row['barcode2']].append(row['flow'])
            slope_lists_low[row['barcode1']].append(row['slope1'])
            slope_lists_low[row['barcode2']].append(row['slope2'])
        else:
            flow_lists_high[row['barcode1']].append(row['flow'])
            flow_lists_high[row['barcode2']].append(row['flow'])
            slope_lists_high[row['barcode1']].append(row['slope1'])
            slope_lists_high[row['barcode2']].append(row['slope2'])
    
    # Determine flow limits for curves (same for all flowcells)
    flow_limits[0] = min(flow_lists_low[barcodes[0]])
    flow_limits[1] = max(flow_lists_low[barcodes[0]])
    if is_dual_curve:
        flow_limits[2] = max(flow_lists_high[barcodes[0]])
    low_flow_min, low_flow_max, high_flow_max = flow_limits
    
    for barcode in barcodes:
        # Calculate calibration parameters
        calib_params_low[barcode] = get_coefficients_polynomial(
            slope_lists_low[barcode],
            flow_lists_low[barcode],
            degree_polynomial_low_flows,
            use_constraint_dec,
            use_constraint_conv
        )
        if is_dual_curve:
            calib_params_high[barcode] = get_coefficients_polynomial(
                slope_lists_high[barcode],
                flow_lists_high[barcode],
                degree_polynomial_high_flows,
                use_constraint_dec,
                use_constraint_conv
            )
        # Determine slope limits by mapping flow limits onto slopes (with inverse polynomial fit)
        params_inverse_low = np.polynomial.polynomial.polyfit(flow_lists_low[barcode], slope_lists_low[barcode], degree_polynomial_low_flows)
        slope_low_flow_min = np.polynomial.polynomial.polyval(low_flow_min, params_inverse_low)
        slope_low_flow_max = np.polynomial.polynomial.polyval(low_flow_max, params_inverse_low)
        slope_limits[barcode] = [slope_low_flow_min, slope_low_flow_max, -1]
        if is_dual_curve:
            params_inverse_high = np.polynomial.polynomial.polyfit(flow_lists_high[barcode], slope_lists_high[barcode], degree_polynomial_high_flows)
            slope_high_flow_max = np.polynomial.polynomial.polyval(high_flow_max, params_inverse_high)
            slope_limits[barcode][2] = slope_high_flow_max
    return flow_lists_low, slope_lists_low, calib_params_low, flow_lists_high, slope_lists_high, calib_params_high, flow_limits, slope_limits


st.sidebar.text('Metadata:')
st.sidebar.text(metadata.replace('# ', ''))

st.subheader('Calibration data')

degree_polynomial_low_flows = st.number_input('Choose the degree of the polynomial fit', 1, 4, value=4)
if st.checkbox('Set different degree for high flows', value=False):
    degree_polynomial_high_flows = st.number_input('Choose a different degree for the polynomial fit for high flows', 1, 4, value=4)
else:
    degree_polynomial_high_flows = degree_polynomial_low_flows

use_constraint_decreasing = st.checkbox('Apply constraint to make polynomial monotonically decreasing', value=False)
## NOTE: constraint to make polynomial convex does not work well with the polyfit library, hence it's always disabled
# use_constraint_convex = st.checkbox('Apply constraint to make polynomial curve convex', value=False)
use_constraint_convex = False

show_raw_data = st.checkbox('Show dataframe with raw data')
if show_raw_data:
    df

nb_groups = math.ceil(len(barcodes) / 10)
plot_barcode = st.select_slider(
    'Plot calibration curves',
    options = ['None'] + barcodes + [f'Group of 10 number {i+1}/{nb_groups}' for i in range(nb_groups)] + ['All'],
    value = 'None',
    help = 'Select the flowcell IDs for which to plot the calibration curve. Use the left and right arrow keys for quick navigation.'
)

flow_lists_low, slope_lists_low, calib_params_low, flow_lists_high, slope_lists_high, calib_params_high, flow_limits, slope_limits = process_data(df, degree_polynomial_low_flows, degree_polynomial_high_flows, use_constraint_decreasing, use_constraint_convex)


def plot_curve(barcode, single_plot=True):
    params_low = calib_params_low[barcode]
    flows_low = flow_lists_low[barcode]
    slopes_low = slope_lists_low[barcode]
    params_high = calib_params_high[barcode]
    flows_high = flow_lists_high[barcode]
    slopes_high = slope_lists_high[barcode]
    slope_low_flow_min, slope_low_flow_max, slope_high_flow_max = slope_limits[barcode]
    
    xx_low = list(np.linspace(slope_low_flow_max, slope_low_flow_min, 50))
    yy_low = [np.dot(params_low, [1, x, x**2, x**3, x**4]) for x in xx_low]
    if is_dual_curve:
        xx_high = list(np.linspace(slope_high_flow_max, slope_low_flow_max, 50))
        yy_high = [np.dot(params_high, [1, x, x**2, x**3, x**4]) for x in xx_high]
    
    if single_plot:
        plt.title(f'Calibration flowcell {barcode}')
        plt.plot(slopes_low, flows_low, 'x', label='Calibration points low flows curve', markersize=8, color='tab:blue')
        plt.plot(xx_low, yy_low, '--', label='Calibration curve for low flows', color='tab:blue')
        if is_dual_curve:
            plt.plot(slopes_high, flows_high, '+', label='Calibration points high flows curve', markersize=10, color='tab:orange')
            plt.plot(xx_high, yy_high, '--', label='Calibration curve for high flows', color='tab:orange')
            plt.plot(slope_limits[barcode], flow_limits, '.', label='Limit values (curve ends)', color='tab:red')
        else:
            plt.plot(slope_limits[barcode][:2], flow_limits[:2], 'r.', markersize=7, label='Limit values (curve ends)')
    else:
        if is_dual_curve:
            plt.plot(xx_high+xx_low, yy_high+yy_low, label=f'{barcode}', alpha=0.7)
        else:
            plt.plot(xx_low, yy_low, label=f'{barcode}', alpha=0.7)

fig = plt.figure()
if plot_barcode == 'None':
    pass
elif plot_barcode == 'All':
    for id in barcodes:
        plot_curve(id, single_plot=False)
elif 'Group' in str(plot_barcode):
    temp = plot_barcode.split(' ')[-1]
    nb_group = int(temp.split('/')[0])
    for id in barcodes[10*nb_group-10:10*nb_group]:
        plot_curve(id, single_plot=False)
else:
    plot_curve(int(plot_barcode))

if plot_barcode != 'None':
    plt.xlabel('Slope (m°C/s)')
    plt.ylabel('Flow (normal l/h)')
    plt.legend()
    plt.grid()
    st.pyplot(fig)


st.subheader('Select new locations (optional)')

experiment, system, rack, position = input_location(st)
location_id = get_location_id(experiment, system, rack, position)

arr_locs = np.empty((2,18), dtype=object)
column_names = [f'CH{i+1}' for i in range(18)]
df_locations = pd.DataFrame.from_records(arr_locs, index=['Input', 'Output'], columns=column_names)

column_config_dict = dict()
for name in column_names:
    column_config_dict[name] = st.column_config.SelectboxColumn(options=barcodes)

st.text('Choose the new location of the flowcell(s) here:')
df_locations = st.data_editor(
    df_locations,
    column_config=column_config_dict
)
all_entries = df_locations.values.ravel().tolist()
barcode_entries = sorted([entry for entry in all_entries if entry != None])
if len(barcode_entries) > len(set(barcode_entries)):
    duplicates = list(set([x for x in barcode_entries if barcode_entries.count(x) > 1]))
    error_display_string = 'barcode '+str(duplicates[0]) if len(duplicates)==1 else 'barcodes '+str(duplicates)
    st.error(f'Duplicate flowcell {error_display_string} detected! Choose at most 1 location for every flowcell!')
    st.stop()

new_flowcell_locations = dict() # Map flowcells to their new location
for row_index in ['Input', 'Output']:
    for channel_index, column_name in enumerate(column_names):
        barcode = df_locations.loc[row_index][column_name]
        if barcode != None:
            new_flowcell_locations[barcode] = (channel_index+1, row_index=='Input')


@st.dialog('Warning!')
def warning_update_db(full_update):
    '''Display a warning dialog before updating the database.
    If the argument full_update is True, then update both locations and calibrations.
    Otherwise, only add new calibrations to the database.'''
    st.write('Are you sure you want to update the database?')
    st.write('The only way to revert this action is to manually edit the database.')
    st.session_state.full_update_db_continue = False
    st.session_state.calibration_update_db_continue = False
    if st.button('Update database'):
        if full_update:
            st.session_state.full_update_db_continue = True
        else:
            st.session_state.calibration_update_db_continue = True
        st.rerun()
    if st.button('Cancel'):
        st.rerun()

def add_calibration_to_database(flowcell_id):
    '''Add the new calibration to the database for the given flowcell ID.
    Return the calibration ID of the new row in the database.'''
    add_flowcell(flowcell_id)
    if is_dual_curve:
        new_calibration_id = add_calibration(
            flowcell_id=flowcell_id,
            timestamp=calibration_timestamp,
            calibration_gas=calibration_gas,
            low_flow_min = round(flow_limits[0], 1),
            low_flow_max = round(flow_limits[1], 1),
            slope_low_flow_min = slope_limits[flowcell_id][0],
            slope_low_flow_max = slope_limits[flowcell_id][1],
            low_x0 = calib_params_low[flowcell_id][0],
            low_x1 = calib_params_low[flowcell_id][1],
            low_x2 = calib_params_low[flowcell_id][2],
            low_x3 = calib_params_low[flowcell_id][3],
            low_x4 = calib_params_low[flowcell_id][4],
            high_flow_enabled = is_dual_curve,
            high_flow_max = round(flow_limits[2], 1),
            slope_high_flow_max = slope_limits[flowcell_id][2],
            high_x0 = calib_params_high[flowcell_id][0],
            high_x1 = calib_params_high[flowcell_id][1],
            high_x2 = calib_params_high[flowcell_id][2],
            high_x3 = calib_params_high[flowcell_id][3],
            high_x4 = calib_params_high[flowcell_id][4],
            config_delta_temp=config_delta_temp
        )
    else:
        new_calibration_id = add_calibration(
            flowcell_id=flowcell_id,
            timestamp=calibration_timestamp,
            calibration_gas=calibration_gas,
            low_flow_min = round(flow_limits[0], 1),
            low_flow_max = round(flow_limits[1], 1),
            slope_low_flow_min = slope_limits[flowcell_id][0],
            slope_low_flow_max = slope_limits[flowcell_id][1],
            low_x0 = calib_params_low[flowcell_id][0],
            low_x1 = calib_params_low[flowcell_id][1],
            low_x2 = calib_params_low[flowcell_id][2],
            low_x3 = calib_params_low[flowcell_id][3],
            low_x4 = calib_params_low[flowcell_id][4],
            high_flow_enabled = is_dual_curve,
            config_delta_temp=config_delta_temp
        )
    logging.info(f'Added calibration for flowcell {flowcell_id}')
    return new_calibration_id


st.subheader('Update database')

if not check_password(st):
    st.stop()  # Do not continue if check_password is not True.

st.markdown(f'''
   Press the button below to update the calibration and location for {len(barcode_entries)} flowcell{"s" if len(barcode_entries) != 1 else ""}{":" if len(barcode_entries) > 0 else "."}
   
   {', '.join([str(b) for b in barcode_entries])}
   
   If flowcells are already installed in the selected locations, their location is changed to building 256.
''')


if 'full_update_db_continue' in st.session_state and st.session_state.full_update_db_continue:
    st.session_state.full_update_db_continue = False
    
    logging.info('Performing full database update (calibrations and locations)...')
    location_id_256 = execute_command_db_return_1("SELECT id FROM public.locations WHERE experiment = '256'")
    
    ## Iterate over flowcells for which a new channel location was selected:
    for flowcell_id, dict_value in new_flowcell_locations.items():
        logging.info(f'Updating database for flowcell {flowcell_id}')
        
        ## Move existing flowcells in selected locations to 256
        channel_nb, is_input = dict_value
        df_flowcell_old = execute_command_db_return_df(
            f'select * from latest_states_flowcell lsf where lsf.location_id = {location_id} and lsf.channel = {channel_nb} and lsf.is_input = {is_input}'
        )
        if len(df_flowcell_old) != 0:
            id_flowcell_old = df_flowcell_old['flowcell_id'][0]
            move_flowcell(
                flowcell_id=id_flowcell_old,
                timestamp=datetime.now() - timedelta(seconds=5),
                location_id=location_id_256,
                channel=channel_nb,
                is_input=is_input
            )
            logging.info(f"Moved existing flowcell {id_flowcell_old} in channel {channel_nb} {'input' if is_input else 'output'} to B256")
        

        ## Add calibration to database
        new_calibration_id = add_calibration_to_database(flowcell_id)
        
        # TODO add raw calibration data to database (raw data is in "df"). For this, use function add_calibration_raw_data
        
        ## Update state of flowcell with new location and calibration
        add_state_flowcell(
            flowcell_id=flowcell_id,
            timestamp=datetime.now(),
            location_id=location_id,
            active_calibration_id=new_calibration_id,
            channel=channel_nb,
            is_input=is_input
        )
        logging.info(f'Updated location for flowcell {flowcell_id}')
        
    logging.info('Finished full database update (calibrations and locations)!')


if 'calibration_update_db_continue' in st.session_state and st.session_state.calibration_update_db_continue:
    st.session_state.calibration_update_db_continue = False
    
    logging.info('Performing calibration database update...')
    for flowcell_id in barcodes:
        add_calibration_to_database(flowcell_id)
    logging.info('Finished calibration database update!')


if st.button('Update database with new locations and calibrations', type='primary'):
    warning_update_db(full_update=True)

st.markdown(f'Alternatively, use the button below to only add the new calibrations for all {len(barcodes)} flowcells to the database.')
if st.button('Update database with new calibrations', type='primary'):
    warning_update_db(full_update=False)
