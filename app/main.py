import streamlit as st

page_read = st.Page(
    'page_read_database.py',
    title='Read database',
    url_path='read_database',
    icon=':material/analytics:'
)
page_write = st.Page(
    'page_write_database.py',
    title='Update database after calibration',
    url_path='update_database',
    icon=':material/edit:'
)
page_move = st.Page(
    'page_move_flowcell.py',
    title='Move flowcell',
    url_path='move_flowcell',
    icon=':material/monitoring:'
)
page_move_elmb = st.Page(
    'page_move_elmb.py',
    title='Move ELMB',
    url_path='move_elmb',
    icon=':material/move_down:'
)
page_move_motherboard = st.Page(
    'page_move_motherboard.py',
    title='Move motherboard',
    url_path='move_motherboard',
    icon=':material/move_down:'
)


pg = st.navigation([page_read, page_write, page_move, page_move_elmb, page_move_motherboard])
st.set_page_config(page_title="Flowcell DB interface", page_icon=':material/database:')
pg.run()
