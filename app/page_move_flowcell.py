import streamlit as st
from utils import *
from datetime import timedelta
from textwrap import wrap


st.subheader('Choose flowcell')
barcode = st.number_input(
    'Barcode',
    min_value=0,
    value=None
)

if barcode == None:
    st.warning('Enter a barcode to continue')
    st.stop()


df_flowcell_current_state = execute_command_db_return_df(f'select * from latest_states_flowcell where flowcell_id = {barcode}')
if len(df_flowcell_current_state) == 0:
    st.markdown('This flowcell has no location history.')
else:
    if st.checkbox('Show details about current location and calibration', value=False):
        current_location_id = df_flowcell_current_state['location_id'][0]
        experiment_current, system_current, rack_current, position_current = get_location_attributes(current_location_id)
        channel_nb_current = df_flowcell_current_state['channel'][0]
        is_input_current = df_flowcell_current_state['is_input'][0]
        st.markdown(f"This flowcell is currently installed in {experiment_current} {system_current}, rack {rack_current} position {position_current}, channel {channel_nb_current} {'input' if is_input_current else 'output'}. Current calibration:")
        calibration_id_current = df_flowcell_current_state['active_calibration_id'][0]
        st.write(execute_command_db_return_df(f'select * from calibrations where id = {calibration_id_current}'))
    else:
        st.markdown('This flowcell is currently installed.')

df_calibrations = get_calibration_df(barcode)
if len(df_calibrations) == 0:
    st.error('This flowcell does not have any calibrations! Select another flowcell to continue.')
    st.stop()


st.subheader('Choose calibration')

options_calibration = []
for _, row in df_calibrations.iterrows():
    min_flow = row['low_flow_min']
    max_flow = row['high_flow_max'] if row['high_flow_enabled'] else row['low_flow_max']
    calibration_gas = row['calibration_gas'] if row['calibration_gas'] else 'NULL'
    calibration_str = calibration_gas + f' ({min_flow}-{max_flow} L/h) from {row["timestamp"]}'
    options_calibration.append(calibration_str)


selected_calibration_index = st.selectbox(
    'Choose calibration',
    range(len(options_calibration)),
    format_func=lambda x: options_calibration[x]
)

st.markdown('Calibrations for this flowcell:')
df_calibrations.insert(loc=0, column='is_selected', value=False)

calibration_row_selected = df_calibrations.iloc[selected_calibration_index]
df_calibrations.loc[selected_calibration_index, 'is_selected'] = True

df_calibrations


plot_selection_indices = st.multiselect(
    'Plot calibration curves',
    options=range(len(options_calibration)),
    default=selected_calibration_index,
    format_func=lambda x: options_calibration[x]
)

if plot_selection_indices:
    fig = plt.figure()
    for plot_index in plot_selection_indices:
        calibration_row = df_calibrations.iloc[plot_index]
        plot_curve(
            calibration_row,
            single_plot=len(plot_selection_indices)==1,
            custom_plot_label='\n'.join(wrap(options_calibration[plot_index], 30))
        )
    plt.title(f"Calibration{'s' if len(plot_selection_indices)!=1 else ''} flowcell {barcode}")
    show_fig(fig)


st.subheader('Choose new location')

experiment, system, rack, position = input_location(st)
channel_nb = st.number_input('Channel number', 1, 18)
is_input = st.checkbox('Is input flowcell?', True)

location_id = get_location_id(experiment, system, rack, position)

df_flowcell_old = execute_command_db_return_df(
    f'select * from latest_states_flowcell lsf where lsf.location_id = {location_id} and lsf.channel = {channel_nb} and lsf.is_input = {is_input}'
)

if len(df_flowcell_old) > 1:
    st.error(f'WARNING! There are several flowcells in the selected location!\
    \n\nFlowcells in this location: {list(df_flowcell_old["flowcell_id"])}\
    \n\nBefore continuing, move the duplicate flowcell to another location.')
    st.stop()

already_flowcell_installed = False
if len(df_flowcell_old) > 0:
    already_flowcell_installed = True
    flowcell_old = df_flowcell_old['flowcell_id'][0]
    calibration_id_old = df_flowcell_old['active_calibration_id'][0]
    df_calibration_old = execute_command_db_return_df(f'select * from calibrations where id = {calibration_id_old}')


st.subheader('Update database')

if not check_password(st):
    st.stop()  # Do not continue if check_password is not True.

if already_flowcell_installed:
    st.markdown(f'The selected location is currently occupied by flowcell {flowcell_old}. If you continue, this flowcell will be moved to building 256. The calibration of flowcell {flowcell_old}:')
    st.write(df_calibration_old)
else:
    st.markdown('The selected location currently has no flowcell.')

st.markdown(f'''Press the button below to move flowcell {barcode} to {experiment} {system}, rack {rack} position {position}, channel {channel_nb} {'input' if is_input else 'output'} with the calibration selected above.
''')


@st.dialog('Warning!')
def warning_update_db():
    st.write('Are you sure you want to update the database?')
    st.session_state.update_calib_db_continue = False
    if st.button('Update database'):
        st.session_state.update_calib_db_continue = True
        st.rerun()
    if st.button('Cancel'):
        st.rerun()

if 'update_calib_db_continue' in st.session_state and st.session_state.update_calib_db_continue:
    st.session_state.update_calib_db_continue = False
    
    if already_flowcell_installed:
        ## Move old flowcell to 256
        location_id_256 = execute_command_db_return_1("SELECT id FROM public.locations WHERE experiment = '256'")
        add_state_flowcell(
          flowcell_id=flowcell_old,
          timestamp=datetime.now() - timedelta(seconds=5),
          location_id=location_id_256,
          active_calibration_id=calibration_id_old
        )
    
    ## Move selected flowcell to selected location with selected calibration
    add_state_flowcell(
        flowcell_id=barcode,
        timestamp=datetime.now(),
        location_id=location_id,
        active_calibration_id=calibration_row_selected['id'],
        channel=channel_nb,
        is_input=is_input
    )

if st.button('Move flowcell', type='primary'):
    warning_update_db()
