import streamlit as st
from utils import *
from datetime import timedelta

if not check_password(st):
    st.stop()  # Do not continue if check_password is not True.

@st.dialog('Warning!')
def warning_add_motherboard():
    '''Display a warning dialog before adding a motherboard to the database.'''
    st.write('Are you sure you want to update the database?')
    st.write('The only way to revert this action is to manually edit the database.')
    st.session_state.add_motherboard_continue = False
    if st.button('Update database'):
        st.session_state.add_motherboard_continue = True
        st.rerun()
    if st.button('Cancel'):
        st.rerun()

@st.dialog('Warning!')
def warning_move_motherboard():
    '''Display a warning dialog before moving motherboards in the database.'''
    st.write('Are you sure you want to update the database?')
    st.write('The only way to revert this action is to manually edit the database.')
    st.session_state.move_motherboard_continue = False
    if st.button('Update database'):
        st.session_state.move_motherboard_continue = True
        st.rerun()
    if st.button('Cancel'):
        st.rerun()


st.subheader('Specify motherboard')

motherboard_id = st.text_input('Motherboard ID (sticker)')
command = f"select count(*) from public.motherboards where id_sticker = '{motherboard_id}'"
count_motherboards = execute_command_db_return_1(command)


if count_motherboards == 0:
    st.write('The database does not contain this motherboard yet.')
    
    if st.button('Add motherboard to database', type='primary'):
        warning_add_motherboard()
    
    if 'add_motherboard_continue' in st.session_state and st.session_state.add_motherboard_continue:
        st.session_state.add_motherboard_continue = False
    
        command = f"INSERT INTO public.motherboards (id_sticker) VALUES ('{motherboard_id}')"
        execute_command_db(command)
        st.rerun()
    
    st.stop()

st.write('The database already contains this motherboard.')

st.subheader('Motherboard location')
experiment, system, rack, position = input_location(st)
location_id = get_location_id(experiment, system, rack, position)

st.subheader('Link to elog')
elog_hyperlink = st.text_input(label='Optionally provide a link to the corresponding elog entry')
if len(elog_hyperlink) == 0:
    elog_hyperlink = 'NULL'

st.markdown('Press the button below to update the database with the new motherboard. The old motherboard in the given location will be moved to 256.')
if st.button('Move motherboard', type='primary'):
    warning_move_motherboard()

if 'move_motherboard_continue' in st.session_state and st.session_state.move_motherboard_continue:
    st.session_state.move_motherboard_continue = False
    
    # Move old motherboard to 256
    df_motherboard_old = execute_command_db_return_df(f'SELECT motherboard_id from latest_states_motherboard where location_id = {location_id}')
    if len(df_motherboard_old) > 0:
        motherboard_id_old = df_motherboard_old['motherboard_id'][0]
        location_id_256 = execute_command_db_return_1("SELECT id FROM public.locations WHERE experiment = '256'")
        execute_command_db(f"INSERT INTO public.states_motherboard (motherboard_id, timestamp, location_id, elog_hyperlink) VALUES \
            ('{motherboard_id_old}', '{datetime.now() - timedelta(seconds=5)}', {location_id_256}, '{elog_hyperlink}')")
    
    # Move new motherboard
    execute_command_db(f"INSERT INTO public.states_motherboard (motherboard_id, timestamp, location_id, elog_hyperlink) VALUES \
        ('{motherboard_id}', '{datetime.now()}', {location_id}, '{elog_hyperlink}')")
