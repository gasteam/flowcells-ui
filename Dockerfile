FROM python:3.11

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
COPY app/ /app/
WORKDIR /app

EXPOSE 8501

ENTRYPOINT ["python3", "-m", "streamlit", "run", "main.py", "--server.port=8501", "--server.address=0.0.0.0"]
