# User interface for the flowcells database

Web interface for the flowcells database built using Streamlit.
The flowcells database is a PostgreSQL database hosted by CERN's DBOD service. The database is hosted on `dbod-flowcells.cern.ch:6604`.
The production website is hosted on the CERN network here: [https://flowcells-ui.app.cern.ch/](https://flowcells-ui.app.cern.ch/)

The web interface is has menus for reading and updating the database covering most common use cases:
- Inspection of current flowcell calibration and ELMB CAN node IDs
- Comparison of different calibrations for a single flowcell (in "Move flowcell" tab)
- Updating database after flowcell recalibration
- Updating database after replacement of ELMB and/or motherboard

To edit the database, a password is needed. This password is mainly there to prevent human mistakes, not for providing a high level of security. The current password is `gas`.

To run locally, use one of the commands below. For the first command, the passwords should be saved locally in a file `.streamlit/secrets.toml` ([see docs](https://docs.streamlit.io/develop/api-reference/connections/secrets.toml)). For the second command, the passwords should be set as environment variables.

- `streamlit run app/main.py use_streamlit_secrets`
- `streamlit run app/main.py`
